#!/bin/bash
#
# experior_monit.sh
# created by Paulo Victor Maluf - 05/2019
#
# Parameters:
#
#   experior_monit.sh --help
#
#    Parameter           Short Description                                                         Default
#    ------------------- ----- ------------------------------------------------------------------- ---------------------------
#    --dxtoolkit-path       -p [OPTIONAL] Path for dxtoolkit scripts                               /var/opt/delphix/dxtoolkit
#    --company              -c [REQUIRED] Client company name
#    --delphix-engine       -d [REQUIRED] Delphix engine hostname or ip address
#    --tmp-dir              -t [OPTIONAL] Temporary directory to store metrics                     /tmp/experior/
#    --start-date           -s [OPTIONAL] Format "YYYY-MM-DD [HH24:MI:SS]"                         last execution or 1 week
#    --end-date             -e [OPTIONAL] Format "YYYY-MM-DD [HH24:MI:SS]"                         current date and time
#    --sender-email         -S [OPTIONAL] From (sender) email address                              suporte@experiortec.com
#    --sender-relay         -r [OPTIONAL] SMTP email relay                                         localhost
#    --sender-port          -P [OPTIONAL] SMTP relay port                                          25
#    --create-conf          -C [OPTIONAL] Create dxtools.conf from csv file
#    --help                 -h [OPTIONAL] 
#    --verbose              -v [OPTIONAL] Turns verbose mode on
#    --version              -V [OPTIONAL] shows script version
#
#   Ex.: experior_monit.sh --dxtoolkit-path /var/opt/delphix/dxtoolkit --company Experior --delphix-engine Delphix -r relay.domain -P 25
#
# Changelog:
#
# Date       Author               Description
# ---------- ------------------- ----------------------------------------------------
#====================================================================================

################################
# VARIAVEIS GLOBAIS            #
################################
DXTOOLKIT="/var/opt/delphix/dxtoolkit"
TMPDIR="/tmp/experior"
SCRIPT_DIR=`cd "$(dirname "$0")" ; pwd -P`
SCRIPT_NAME=`basename $0`
SCRIPT_LOGDIR="${SCRIPT_DIR}/logs"
LOG="${SCRIPT_LOGDIR}/${SCRIPT_NAME/.sh/.log}"
LAST="${SCRIPT_LOGDIR}/.last"
MAIL_LST="suporte@experiortec.com"
SENDER="suporte@experiortec.com"
SENDEMAIL="${SCRIPT_DIR}/sendEmail"
SMTPORT="25"
SMTPRELAY="localhost"
DXTOOLSCSV="${SCRIPT_DIR}/dxtools.csv"

SCRIPTVERSION="0.0.1"
################################
# FUNCOES                      #
################################
help(){
  head -27 $0 | tail -23
  exit
}

log(){
 MSG=$1
 if [ "${VERBOSE}." == "True." ]
  then
    echo -ne "[`date "+%F %T"`] ${MSG}" | tee -a ${LOG}
  else 
    echo -ne "[`date "+%F %T"`] ${MSG}" >> ${LOG}
fi
}

version(){
  log "${SCRIPT_NAME} ${SCRIPTVERSION}"
  exit 0
}

get_config(){
  log "Getting engine configuration...\n"
  ${DXGETCONFIG} -d "${DXENGINE}_sysadmin" -format csv -configfile ${DXTOOLKITCONF} > ${TMPDIR}/${COMPANY}/${DXENGINE}_config.csv
  [ "$?." != "0." ] && { log "ERROR: Failed to get engine configuration!\n" ;} 
}

get_analytics(){
  log "Getting analytics metrics...\n"
  ${DXGETANALYTIC} -d ${DXENGINE} -i 60 -t standard -st ${STARTIME} -et ${ENDTIME} -outdir "${TMPDIR}/${COMPANY}"
  [ "$?." != "0." ] && { log "ERROR: Failed to get analytics metrics!\n" ;}
}

get_events(){
  log "Getting engine events...\n"
  ${DXGETEVENT} -d ${DXENGINE} -st ${STARTIME} -et ${ENDTIME} -format csv -outdir "${TMPDIR}/${COMPANY}" 
  [ "$?." != "0." ] && { log "ERROR: Failed to get events!\n" ;}
}

get_faults(){
  log "Getting engine faults...\n"
  ${DXGETFAULT} -d ${DXENGINE} -st ${STARTIME} -et ${ENDTIME} -format csv -outdir "${TMPDIR}/${COMPANY}" 
  [ "$?." != "0." ] && { log "ERROR: Failed to get faults!\n" ;}
}

get_capacity(){
  log "Getting engine capacity and space usage...\n"
  ${DXGETCAPACITY} -d ${DXENGINE} -details -st ${STARTIME} -et ${ENDTIME} -format csv > ${TMPDIR}/${COMPANY}/${DXENGINE}_capacity.csv
  [ "$?." != "0." ] && { log "ERROR: Failed to get engine capacity metrics!\n" ;}
}

get_appliance(){
  log "Getting engine appliance infos...\n"
  ${DXGETAPPLIANCE} -d ${DXENGINE} -details -format csv > ${TMPDIR}/${COMPANY}/${DXENGINE}_appliance.csv
  [ "$?." != "0." ] && { log "ERROR: Failed to get appliance infos!\n" ;}
}

get_dbenv(){
  log "Getting engine environment info...\n"
  ${DXGETDBENV} -d ${DXENGINE} -format csv > ${TMPDIR}/${COMPANY}/${DXENGINE}_dbenv.csv
  [ "$?." != "0." ] && { log "ERROR: Failed to get appliance infos!\n" ;}
}

create_dxtools(){
  CHK=`grep -v "^#" ${DXTOOLSCSV} | wc -l`
  if [ ${CHK} -gt 0 ]
    then
      log "Creating dxtools.conf from ${DXTOOLSCSV}\n"
      ${DXCONFIG} -convert todxconf -csvfile ${DXTOOLSCSV} -configfile "${DXTOOLKIT}/dxtools.conf"
      [ "$?." != "0." ] && { log "ERROR: Failed to convert csv ${DXTOOLSCSV} to  ${DXTOOLKIT}/dxtools.conf!\n" ; exit 1 ;}
    else
      log "ERROR: Invalid configuration in ${DXTOOLSCSV}\n"
      exit 1
  fi
}

package(){
  log "Packing all retrieved data...\n"
  cd ${TMPDIR}
  zip ${COMPANY}_analytics_${DXENGINE}.zip ${COMPANY}/*
  [ "$?." != "0." ] && { log "ERROR: Packing metrics failed!\n" ; exit 1 ;}
}

sendmail(){
  ${SENDEMAIL} -u "[${COMPANY}] Analytics Report" -f ${SENDER} -t "${MAIL_LST}" -a "${TMPDIR}/${COMPANY}_analytics_${DXENGINE}.zip" -m "Analytics Report" -s ${SMTPRELAY}:${SMTPORT}
}

purge(){
  log "Removing temporary files...\n"
  rm -f ${TMPDIR}/${COMPANY}_analytics_${DXENGINE}.zip ${TMPDIR}/${COMPANY}/*.csv
  [ "$?." != "0." ] && { log "ERROR: Unable to remove temporary files!\n" ; exit 1 ;}

  rm -rf ${TMPDIR}/cookies.* ${TMPDIR}/${DXENGINE}/* ${TMPDIR}/par-*
  [ "$?." != "0." ] && { log "ERROR: Unable to remove temporary files!\n" ; exit 1 ;}
}

# Parameters
for arg
do
    delim=""
    case "$arg" in
    #translate --gnu-long-options to -g (short options)
      --dxtoolkit-path)  args="${args}-p ";;
      --delphix-engine)  args="${args}-d ";;
      --company)         args="${args}-c ";;
      --create-conf)     args="${args}-C ";;
      --tmp-dir)         args="${args}-t ";;
      --start-date)      args="${args}-s ";;
      --end-date)        args="${args}-e ";;
      --sender-email)    args="${args}-S ";;
      --smtp-relay )     args="${args}-r ";;
      --smtp-port )      args="${args}-P ";;
      --version)         args="${args}-V ";;
      --verbose)         args="${args}-v ";;
      --help)            args="${args}-h ";;
      #pass through anything else
      *) [[ "${arg:0:1}" == "-" ]] || delim="\""
         args="${args}${delim}${arg}${delim} ";;
    esac
done

eval set -- $args

while getopts ":hVvCd:c:t:e:s:p:S:r:P" PARAMETRO
do
    case $PARAMETRO in
        h) help;;
        p) DXTOOLKIT=${OPTARG[@]};;
        c) COMPANY=${OPTARG[@]};;
        C) CREATE_CONF="True";;
        t) TMPDIR=${OPTARG[@]};;
        d) DXENGINE=${OPTARG[@]};;
        s) STARTIME=${OPTARG[@]};;
        S) SENDER=${OPTARG[@]};;
        e) ENDTIME=${OPTARG[@]};;
        r) SMTPRELAY=${OPTARG[@]};;
        P) SMTPORT=${OPTARG[@]};;
        V) version;;
        v) VERBOSE="True";;
        :) echo "Option -$OPTARG requires an argument."; exit 1;;
        *) echo $OPTARG is an unrecognized option ; echo $USAGE; exit 1;;
    esac
done

[ "$1" ] || help
[ -d ${SCRIPT_LOGDIR} ] || mkdir -p ${SCRIPT_LOGDIR}
[ -d ${TMPDIR}/${COMPANY} ] || mkdir -p ${TMPDIR}/${COMPANY}
[ -x ${SENDEMAIL} ] || chmod +x ${SENDEMAIL}

#########################
# Main                  #
#########################
DXTOOLKITCONF="${DXTOOLKIT}/dxtools.conf"
DXCONFIG="${DXTOOLKIT}/dx_config"
DXGETCAPACITY="${DXTOOLKIT}/dx_get_capacity_history"
DXGETANALYTIC="${DXTOOLKIT}/dx_get_analytics"
DXGETEVENT="${DXTOOLKIT}/dx_get_event"
DXGETFAULT="${DXTOOLKIT}/dx_get_faults"
DXGETCONFIG="${DXTOOLKIT}/dx_get_config"
DXGETAPPLIANCE="${DXTOOLKIT}/dx_get_appliance"
DXGETDBENV="${DXTOOLKIT}/dx_get_db_env"

# Check if dxtoolkit is installed
if ! [ -d "${DXTOOLKIT}" ]; then
  log "Error: ${DXTOOLKIT} is not found. "
  exit 1
fi

# Create the dxtools.conf from csv
[ "${CREATE_CONF}." == "True." ] && create_dxtools 
 
if [ `uname` == 'Linux' ]
  then
    # Linux Date
    [ -f ${LAST} ] && STARTIME=`cat ${LAST}` || STARTIME=`date "+%F %T" -d "-7 days"`
  elif [ `uname` == 'Darwin' ]
    then
      # MACOSX
      [ -f ${LAST} ] && { STARTIME=`cat ${LAST}` ;} || STARTIME=`date -v -7d "+%F %T"`
  else 
    log "Error: Operation system `uname` not supported"
fi
ENDTIME=`date "+%F %T"`

log "Starting getting metrics from ${STARTIME} until ${ENDTIME}\n"
get_config
get_analytics
get_events
get_faults
get_capacity
get_appliance
get_dbenv
package
sendmail
purge

echo ${ENDTIME} > ${LAST}
